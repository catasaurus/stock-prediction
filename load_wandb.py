import wandb
import pandas as pd

api = wandb.Api()
runs = api.runs("sebastianlarson/stock-prediction-tangible")

data = []

for run in runs:
  new_append = run.summary._json_dict
  data.append(new_append)

print(data["coef"])


