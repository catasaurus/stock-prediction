import pandas as pd
import yfinance as yf
from sklearn.linear_model import LinearRegression
import numpy as np
import matplotlib.pyplot as plt

# split is the percentage of the dataframe that should be used for training < 1
def split(data: pd.DataFrame, split: int, y_cols: list[str], exclude_cols: list) -> tuple[pd.DataFrame(), pd.DataFrame(), pd.DataFrame(), pd.DataFrame()]:
    '''
    Split dataframe in X training set, X testing set, y training set, and y testing set

    >>> len(split(preprocess(), split=0.8, y_cols=['AAPL_Open'], exclude_cols=[]))
    4

    '''
    split_index = int(len(data) * split)

    y = data[y_cols]
    X = data.drop(y_cols + exclude_cols, axis=1)

    X_train = X.iloc[:split_index]
    X_test = X.iloc[split_index:]

    y_train = y.iloc[:split_index]
    y_test = y.iloc[split_index:]
        
    return X_train, X_test, y_train, y_test

def load_stocks(tickers=['AAPL']):
    '''
    loads stocks

    >>> df = load_stocks()
    >>> df
                                AAPL_Open   AAPL_High    AAPL_Low  AAPL_Close  AAPL_Volume  ...  AMZN_Close  AMZN_Volume AMZN_Dividends  AMZN_Stock Splits                AMZN_today
    Date                                                                                    ...                                                                                     
    2003-07-22 00:00:00-04:00    0.316756    0.318122    0.311139    0.315693    198424800  ...    1.743500    317936000            0.0                0.0 2003-07-22 00:00:00-04:00
    ...                               ...         ...         ...         ...          ...  ...         ...          ...            ...                ...                       ...
    2023-07-21 00:00:00-04:00  194.100006  194.970001  191.320007  191.940002     58364002  ...  130.000000     96482177            0.0                0.0 2023-07-21 00:00:00-04:00

    [5035 rows x 16 columns]
    >>> df.shape
    (5035, 16)
    '''
    df = pd.DataFrame()
    for ticker in tickers:
        t = yf.Ticker(ticker)
        # 240 is 20 years
        t_hist: pd.DataFrame = t.history(period='240mo')
        t_hist['today'] = t_hist.index.to_list()

         # add ticker to every column
        rename = {}
        for c in t_hist.columns:
            if c != 'today':
                rename[c] = ticker + '_' + c
        t_hist = t_hist.rename(columns=rename)

        # add cols to data
        for c in t_hist.columns:
            df[c] = t_hist[c]

    df['ds'] = df.index

    return df


def lag_df(df: pd.DataFrame, lag_num):
    df2 = df.copy()
    for i in range(1, lag_num+1):
        for col in df.columns:
            df2[f"lag_{i}_"+col] = df[col].shift(i)
    return df2

def main():
    df = load_stocks()
    df = df[["AAPL_Close"]]

    df["month"] = df.index.month
    df["day_of_month"] = df.index.day
    df = pd.get_dummies(df, columns=["month", "day_of_month"])

    df["year"] = df.index.year

    df = lag_df(df, 1).dropna()

    # sklearn needs an array of array(s) (removed reshape as there are now several features)
    X_train, X_test, y_train, y_test = split(data=df, split=0.8, y_cols=['AAPL_Close'], exclude_cols=[])

    model = LinearRegression()
    model.fit(X=X_train, y=y_train)

    preds = model.predict(X_train)
    rmse_train = np.sqrt(np.mean((preds - y_train)**2))

    preds = model.predict(X_test)
    rmse_test = np.sqrt(np.mean((preds - y_test)**2))
    
    plotting_df = pd.DataFrame()
    plotting_df["preds"] = preds[:, 0] 
    plotting_df["AAPL_Close"] = y_test.reset_index()["AAPL_Close"]

    plotting_df = plotting_df.set_index(y_test.index)

    return dict(
        df=df,
        plotting_df=plotting_df,
        rmse_train=rmse_train,
        rmse_test=rmse_test,
        model=model,
        coef=model.coef_,
        bias=model.intercept_,
    )

def plot_df(df: pd.DataFrame, use_index: int):
    #print(df.columns)
    if use_index:
        plt.plot(df.index, df.preds, "b+")
        plt.plot(df.index, df.AAPL_Close, "r+")
        plt.grid()
        plt.show()
    
    else:
        plt.plot(range(len(df)), df.preds, "b+")                
        plt.plot(range(len(df)), df.AAPL_Close, "r+")
        plt.grid()
        plt.show()




if __name__ == "__main__":
    globals().update(main()) 

df = pd.read_csv("data/train_1.csv", index_col=0)
df = df.T
df = df[["Main_Page_en.wikipedia.org_all-access_all-agents"]]
df["date"]=  df.index.values
df["date"] = pd.to_datetime(df["date"])
df["month"] = df["date"].dt.month
df["day_of_month"] = df["date"].dt.day
