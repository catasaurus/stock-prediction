import pandas as pd
import yfinance as yf
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import sys
import argparse

def load_stocks(tickers=['AAPL', 'AMZN']):
    '''
    loads stocks

    >>> df = load_stocks()
    >>> df
                                AAPL_Open   AAPL_High    AAPL_Low  AAPL_Close  AAPL_Volume  ...  AMZN_Close  AMZN_Volume AMZN_Dividends  AMZN_Stock Splits                AMZN_today
    Date                                                                                    ...                                                                                     
    2003-07-22 00:00:00-04:00    0.316756    0.318122    0.311139    0.315693    198424800  ...    1.743500    317936000            0.0                0.0 2003-07-22 00:00:00-04:00
    ...                               ...         ...         ...         ...          ...  ...         ...          ...            ...                ...                       ...
    2023-07-21 00:00:00-04:00  194.100006  194.970001  191.320007  191.940002     58364002  ...  130.000000     96482177            0.0                0.0 2023-07-21 00:00:00-04:00

    [5035 rows x 16 columns]
    >>> df.shape
    (5035, 16)
    '''
    data = pd.DataFrame()
    for ticker in tickers:
        t = yf.Ticker(ticker)
        # 240 is 20 years
        t_hist: pd.DataFrame = t.history(period='240mo')
        t_hist['today'] = t_hist.index.to_list()

         # add ticker to every column
        rename = {}
        for c in t_hist.columns:
            if c != 'today':
                rename[c] = ticker + '_' + c
        t_hist = t_hist.rename(columns=rename)

        # add cols to data
        for c in t_hist.columns:
            data[c] = t_hist[c]

    return data

def add_share_counts(data: pd.DataFrame, tickers, share_count):
    '''
    adds share count feature to load_data

    >>> df = load_stocks()
    >>> df = add_share_counts(df, ['AMZN', 'AAPL'])
    >>> df
                                AAPL_Open   AAPL_High    AAPL_Low  AAPL_Close  AAPL_Volume  ...  AMZN_Dividends  AMZN_Stock Splits                AMZN_today  AMZN_share_count  AAPL_share_count
    Date                                                                                    ...                                                                                                 
    2003-07-22 00:00:00-04:00    0.316756    0.318122    0.311139    0.315693    198424800  ...             0.0                0.0 2003-07-22 00:00:00-04:00         468762000        5575330000
    ...                               ...         ...         ...         ...          ...  ...             ...                ...                       ...               ...               ...
    2023-07-21 00:00:00-04:00  194.100006  194.970001  191.320007  191.940002     58364002  ...             0.0                0.0 2023-07-21 00:00:00-04:00       10260400128       15728700416

    [5035 rows x 18 columns]
    >>> df.shape
    (5035, 18)
    '''
    for ticker in tickers:
        t = yf.Ticker(ticker)
        # TODO: see if datetime.fromisoformat(str(dt).split(':')[0]) works
        start_date = str(data.index.values[0]).split(':')[0][:-3]
        share_count: pd.Series = t.get_shares_full(start=start_date, end=None)
        # Series is weird to work with (dates)
        share_count2 = pd.DataFrame()
        share_count2['share_count'] = share_count
        share_count2['ds'] = share_count.index.to_list()
        # Share count only changes every so often, and share_count2['share_count'] only has entries for when it changes
        share_count_list = []
        cur = share_count2['share_count'].iloc[0]
        for date in data.index:
            count = share_count2[share_count2['ds'] == date]['share_count']
            if len(count) == 0:
                share_count_list.append(cur)
            else:
                share_count_list.append(count.iloc[0])
                cur = count.iloc[0]
        data[ticker+'_share_count'] = share_count_list
    
    return data

def add_lag_columns(data:pd.DataFrame, lag_num):
    data2 = data
    data2['ds'] = data.index
    cols = data.columns

    for i in range(1, lag_num):
        for column in cols:
            data2[column+'_lag_'+str(i)] = data[column].shift(i)
        data2['ds_lag_'+str(i)] = data2['ds'].shift(i)
        
    return data2


def preprocess(tickers=['AAPL', 'AMZN']):
    '''
    loads yahoo finance data on tickers and processes into dataframe

    index 0 because -1 is the most recent and thus is subject to change
    >>> preprocess().iloc[0]['AAPL_Open']
    0.30370235655595024
    '''
    # not a dict because yfinance returns everything as some sort of pandas object (Series, DataFrame, etc)
    return 

# split is the percentage of the dataframe that should be used for training < 1
def split(data: pd.DataFrame, split: int, y_cols: list[str], exclude_cols: list) -> tuple[pd.DataFrame(), pd.DataFrame(), pd.DataFrame(), pd.DataFrame()]:
    '''
    Split dataframe in X training set, X testing set, y training set, and y testing set

    >>> len(split(preprocess(), split=0.8, y_cols=['AAPL_Open'], exclude_cols=[]))
    4

    '''
    split_index = int(len(data) * split)

    y = data[y_cols]
    X = data.drop(y_cols + exclude_cols, axis=1)

    X_train = X.iloc[:split_index]
    X_test = X.iloc[split_index:]

    y_train = y.iloc[:split_index]
    y_test = y.iloc[split_index:]
        
    return X_train, X_test, y_train, y_test


def process_nans(df: pd.DataFrame, fill_value=0):
    '''
    Processes nans and returns df with no nans

    >>> np.nan in process_nans(preprocess())
    False
    '''
    #drop_columns = [c for c in df.columns if df[c].isna().sum() == len(df)]
    #df = df.drop(columns=drop_columns)
    df_isna = df.isna()
    df_isna.columns = [c + '_isna' for c in df_isna.columns]

    df = pd.concat(objs=[df, df_isna], axis=1)
    df = df.fillna(value=fill_value)
    return df

def synthetic_data(length: int, nan_freq: int) -> pd.DataFrame:
    '''
    Returns sythentic data with columns of complete nans and column with some nans

    >>> len(synthetic_data(length=10, nan_freq=0.3))
    10
    '''
    #data = { 'nans': [np.nan for i in range(length)], 'nan_rand':[] }
    data = {}

    for i in range(length):
        r = np.random.rand()
        if r > nan_freq:
            data['nan_rand'].append(r)
        else:
            data['nan_rand'].append(None)

    return pd.DataFrame(data)

def main(data: pd.DataFrame, train_freq, y_cols, exclude_cols):
    X_train, X_test, y_train, y_test = split(data=data, split=train_freq, y_cols=y_cols, exclude_cols=exclude_cols)
    
    print('Lengths:')
    print('X_train: ', str(len(X_train)))
    print('X_test: ', str(len(X_test)))
    print('y_train: ', str(len(y_train)))
    print('y_test: ', str(len(y_test)))

    #print(X_train.head())
    #print(y_train.head())

    model = LinearRegression()
    model.fit(X=X_train, y=y_train)

    y_pred = model.predict(X=X_test)
    mse = mean_squared_error(y_test, y_pred)

    print('MSE: ' + str(mse))

    return model

def get_shares_count(df: pd.DataFrame, tickers):
    shares_count = pd.DataFrame()
    for ticker in tickers:
        t = yf.Ticker(ticker)
        # TODO: see if datetime.fromisoformat(str(dt).split(':')[0]) works
        start_date = str(df.index.values[0]).split(':')[0][:-3]
        share_count1: pd.Series = t.get_shares_full(start=start_date, end=None)
        # Series is weird to work with (dates)
        share_count2 = pd.DataFrame()
        share_count2['share_count'] = share_count1
        share_count2['ds'] = share_count1.index.to_list()
        # Share count only changes every so often, and share_count2['share_count'] only has entries for when it changes
        share_count_list = []
        cur = share_count2['share_count'].iloc[0]
        for date in df.index:
            count = share_count2[share_count2['ds'] == date]['share_count']
            if len(count) == 0:
                share_count_list.append(cur)
            else:
                share_count_list.append(count.iloc[0])
                cur = count.iloc[0]
        # shares count only named here to keep the existing code working 
        shares_count[ticker+'_shares_count'] = share_count_list

    return shares_count.set_index(df.index)

def concat_columns(*args):
    '''
    concats shares_count and df

    only one line long, maybe get rid of this function and just call standard pd.concat
    '''

    return pd.concat(objs=args, axis=1)

def get_day_features(df: pd.DataFrame):
    # TODO: weekday and week and zero indexed but month and day (day of month) are not. Switch two to be like the other two?
    ds = pd.Series(df.index.values)

    df2 = pd.DataFrame()

    df2['weekday'] = ds.dt.weekday
    df2['month'] = ds.dt.month
    df2['day'] = ds.dt.day
    df2['week'] = [i % 4 for i in ds.dt.isocalendar().week]

    for col in df2.columns:
        new_cols = pd.get_dummies(df2[col], prefix=col)
        df2 = pd.concat(objs=[df2, new_cols], axis=1)

    return df2.set_index(df.index).drop(columns=['weekday', 'month', 'day', 'week'])
'''
df = pd.read_csv('./MMM_AOS_ABT.csv')
df = df.rename(columns={'ds':'today'})

for c in df.columns:
    for i in range(1, 21):
        df[c+f'-{i}'] = df[c].shift(-i)

df.to_csv('./MMM_AOS_ABT_clean.csv', index=False)
'''

'''
As much data as possible on two different stocks.

Create preprocessing script (as a function).

Create training and test set constructor in the same file as a function.

Call both functions in name == main.

X_train X_test y_train y_test.

Train linear regression on X_train and y_train.

Test it on X_test and y_test.

Convert all dates to a datetime object or a pandas timestamp object

Bonus:
Try to extract number from today date.
'''

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--stocks',
        nargs='*',
        type=str,
        default=['AAPL', 'AMZN']
    )
    parser.add_argument(
        '--lag_num',
        nargs=1,
        type=int,
        default=[7]
    )
    parser.add_argument(
        '--train_freq',
        nargs=1,
        type=float,
        default=[0.8]
    )
    parser.add_argument(
        '--y_columns',
        nargs='*',
        type=str,
        default=['AAPL_Open', 'AAPL_High', 'AAPL_Low', 'AAPL_Close', 'AAPL_Volume']
    )
    parser.add_argument(
        '--exclude_columns',
        nargs='*',
        type=str,
        default=['AMZN_today', 'AAPL_today']
    )
    parser.add_argument(
        '--fill_value',
        nargs='*',
        default=0
    )

    '''
    train_freq = 0.8
    y_cols = ['AAPL_Open', 'AAPL_High', 'AAPL_Low', 'AAPL_Close', 'AAPL_Volume']
    exclude_cols = ['AMZN_today', 'AAPL_today']

    if len(sys.argv) > 1:
        train_freq = float(sys.argv[1])
        if len(sys.argv) > 2:
            # TODO: find a replacement for eval
            y_cols = eval(sys.argv[2])
            if len(sys.argv) > 3:
                exclude_cols = eval(sys.argv[3])
    '''

    args = parser.parse_args()

    print('Train frequency: ' + str(args.train_freq[0]))
    print('Y columns: ' + ', '.join(args.y_columns))
    print('Exclude columns: ' + ', '.join(args.exclude_columns))
    print('Stocks: ', ', '.join(args.stocks))
    print('Lags: ' + str(args.lag_num[0]))
    # fill value not printed as any type is hard to handle

    df = load_stocks(tickers=args.stocks)
    df = add_share_counts(df, args.stocks)
    df = add_lag_columns(df, args.lag_num[0])

    print('Columns in DataFrame:')
    print(df.columns)

    df_isna = df.isna()
    drop_cols = []
    for col in df.columns:
        if False not in df_isna[col].to_list():
            drop_cols.append(col)
    
    # removing datetime columns
    date_drop_cols = [col for col in df.columns if 'today' in col]

    drop_cols += date_drop_cols

    # set is used due to potential duplicates
    drop_cols = set(drop_cols)

    print('Dropping columns:')
    print(drop_cols)
    df = df.drop(columns=drop_cols, axis=0)

    print('Columns in DataFrame:')
    print(df.columns)

    df = process_nans(df, args.fill_value)

    # potential duplicates
    exclude_columns = [col for col in args.exclude_columns if col not in drop_cols]
    model = main(df, args.train_freq[0], args.y_columns, args.exclude_columns)
'''
Create test DataFrame that has a column that is all nans and a column that has random nans
Create function to create synthetic nan data for test pipeline
Output DataFrame that has no nans in it and has twice as many columns (*_isna)
Instead of dropna use fillna to fill w/ 0 or 1
Create doctest for each utility function
'''


'''
Add dropping columns that are 100% one value to ML pipeline (unless if become longer than 10 if statements)
Modularize and paramaterize preproccess function (load_stocks(tickers), add_lag_columns(df, lag_range=(0, 1)), main(df, train_freq), add_share_counts(df))
Create main function that takes processed command line args as function args, and dataframe
'''


'''
Make get_shares_count function for creating a second dataframe, takes tickers and stock price dataframe (for date index) - done
Make another another function that combines a share count dataframe with a stock price dataframe - done
Create function function that extracts weekday (cat), month (cat), week of the month (cat), year (int) from datetime object to add to dataframe, use get_dummies for cat
Show scores for two different models and explain why one of them is better or worse then the other (one includes the datetime features, one that doesn't, use rmse)
Add function that normalized and price and gives percent change from first ever price (get rid of original prices)
Add function the normalizes the price and gives today's price divided by yestarday's price (number between 0 and 1) (get rid of original prices)
'''